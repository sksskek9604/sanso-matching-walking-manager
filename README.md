# 산책 메이트 매칭 API

* 산책 메이트 매칭 서비스를 위한 API입니다.

---
###
### LANGUAGE
> ##### - JAVA 16
> ##### - SpringBoot 2.7.3
###

### 기능
> - 로그인 (토큰 기능 부여)
> > 1. 관리자 로그인
> > 2. 회원 로그인
>
> * [관리자/유저] 회원 가입
>
> * 회원정보
> > 1. 닉네임 조회
> > 2. 닉네임 수정
> > 3. [관리자] 회원 정보 리스트 필터 조회
>
>  * 공지사항
> >  1. 공지사항 등록
> >  2. 공지사항 항목 조회
> >  3. [유저용] 공지사항 필터(페이지/기간별) 조회
> >  4. [관리자용] 공지사항 필터(페이지/기간/제목 검색/공지 유무) 조회
> >  5. 공지사항 전체 리스트 조회
> >  6. 공지사항 게시 / 해제
> >  7. 공지사항 수정
>
> * 출퇴근 관리
> >  1. 출근/외출/퇴근 상태 변경
> >  2. 오늘 근태 상태 가져오기
> >  3. 직원별 출퇴근 전체기록 가져오기
>
> * 펫 관리
> > 1. [일반 유저] 펫 등록
> > 2. [일반 유저] 펫 전체 목록 조회
> > 3. [일반 유저] 각 펫의 정보 조회
> > 4. [일반 유저] 펫 이름 수정
> > 5. [일반 유저] 펫 정보 수정
> >
>
> * 키워드 기능 관리
> > 1. 나의 키워드 등록
> > 2. 나의 '나는 이런 친구가 될게요' 리스트 조회
> > 3. 나의 '나는 이런 친구가 좋아요' 리스트 조회
> > 4. 나의 '산책관' 리스트 조회
> > 5. 나의 '산책관' 수정
> > 6. 나의 '나는 이런 친구가 좋아요' 수정
> > 7. 나의 '나는 이런 친구가 될게요' 수정
>
> * 산책 장소 관리
> > 1. [관리자] 산책 장소 등록
> > 2. [관리자] 산책장소 수정
> > 3. [일반 유저] 산책장소 전체 리스트 조회
> > 4. [일반 유저] 나의 즐겨찾기 장소 등록/수정
> > 5. [일반 유저] 나의 즐겨찾기 장소 리스트 조회
>
> > * 매칭 관리
> > 1. [일반 유저] 매칭 신청
> > 2. [일반 유저] 내가 신청한 매칭 내역 조회
> > 3. [일반 유저] 내가 받은 매칭 내역 조회
> > 4. [일반 유저] 매칭 수락


---
## 초기 설계
> ![design1](./images/sanso-initial-design-1.png)
> ![design2](./images/sanso-initial-design-2.png)

## Swagger
> ![swaggerAPI](./images/api-all.png)
> 
> ##### 관리자 자동 생성
> ![superadmin](./images/superadmin-auto-create.png)
> ##### 관리자 토큰 생성
> ![superadmin2](./images/token-create.png)
> > ##### 관리자 토큰 사용
> ![superadmin2](./images/token-use.png)
> 
##### 
> #### 공지사항 API
>
> ![notice](./images/notice-api.png)
> #### 권한 API
>
> ![auth](./images/auth-api.png)
> #### 로그인 API
>
> ![auth](./images/login-api.png)
> #### 매칭내역 API
>
> ![auth](./images/matching-api.png)
> #### 산책장소 API
>
> ![auth](./images/walking-address-api.png)
> #### 키워드 API
>
> ![auth](./images/keyword-api.png)
> #### 파일 관리 API
>
> ![auth](./images/file-api.png)
>
> #### 펫 API
>
> ![auth](./images/pet-api.png)
>
> #### 회원 API
>
> ![auth](./images/member-api.png)
