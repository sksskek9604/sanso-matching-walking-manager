package com.amh.sansomatchingwalkingapi.advice;

import com.amh.sansomatchingwalkingapi.enums.common.ResultCode;
import com.amh.sansomatchingwalkingapi.exception.*;
import com.amh.sansomatchingwalkingapi.model.common.CommonResult;
import com.amh.sansomatchingwalkingapi.service.common.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) { //기본 비상구, 기본 실패하였습니다.
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST) //사용자가 잘못이면 400번으로 보내줌
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CAccessDeniedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED) // 권한이 없습니다. + UNAUTHORIZED = 403번 에러
    protected CommonResult customException(HttpServletRequest request, CAccessDeniedException e) {
        return ResponseService.getFailResult(ResultCode.ACCESS_DENIED);
    }

    @ExceptionHandler(CUsernameSignInFailedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)  // 사용자의 입력의 문제일 가능성이 크다. unauthorized => bad_request
    protected CommonResult customException(HttpServletRequest request, CUsernameSignInFailedException e) {
        return ResponseService.getFailResult(ResultCode.USERNAME_SIGN_IN_FAILED); // 가입된 사용자가 아닙니다.
    }

    @ExceptionHandler(CAuthenticationEntryPoint.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)  // 접근 권한도 권한이 없는 부분이니 unauthorized
    protected CommonResult customException(HttpServletRequest request, CAuthenticationEntryPoint e) {
        return ResponseService.getFailResult(ResultCode.AUTHENTICATION_ENTRY_POINT); // 접근 권한이 없습니다.
    }
    @ExceptionHandler(CUsernameTypeErrorException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CUsernameTypeErrorException e) {
        return  ResponseService.getFailResult(ResultCode.USERNAME_TYPE_ERROR);
    }
    @ExceptionHandler(CLoginPasswordFailedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CLoginPasswordFailedException e) {
        return  ResponseService.getFailResult(ResultCode.LOGIN_PASSWORD_FAILED);
    }
    @ExceptionHandler(CUsernameUsingException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CUsernameUsingException e) {
        return  ResponseService.getFailResult(ResultCode.USERNAME_USING);
    }

    @ExceptionHandler(CWrongPhoneNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CWrongPhoneNumberException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_PHONE_NUMBER);
    }
    @ExceptionHandler(CPhoneNumberUsingException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CPhoneNumberUsingException e) {
        return ResponseService.getFailResult(ResultCode.PHONE_NUMBER_USING);
    }
    @ExceptionHandler(CNickNameUsingException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNickNameUsingException e) {
        return ResponseService.getFailResult(ResultCode.NICKNAME_USING);
    }
    @ExceptionHandler(CNickNameOverlapException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNickNameOverlapException e) {
        return ResponseService.getFailResult(ResultCode.NICKNAME_OVERLAP);
    }
    @ExceptionHandler(CNoEqualCheck.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoEqualCheck e) {
        return ResponseService.getFailResult(ResultCode.NO_EQUAL_CHECK);
    }

    @ExceptionHandler(CApplyMemberOverlapException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CApplyMemberOverlapException e) {
        return ResponseService.getFailResult(ResultCode.APPLY_MEMBER_OVERLAP);
    }
}
