package com.amh.sansomatchingwalkingapi.configure;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration  // 설정값, 처음부터 제공이 되어야해서 bean이랑 거의 비슷한 것.
@RequiredArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter { // 세부 케이스를 여기서 작성하고 제공해줄 수 있게 되는 것이다.
    private final JwtTokenProvider jwtTokenProvider;
    // 시큐리티 설정값을 이제 제공할건데, 어노테이션을 달았는데 뭔가 맥락이 빠졌다 느낀다면 extends webSecurity~

    private static final String[] AUTH_WHITELIST = {
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/v2/api-docs",
            "/webjars/**"
    };

    @Override
    public void configure(WebSecurity webSecurity) throws Exception {
        webSecurity.ignoring().antMatchers(AUTH_WHITELIST);
    }

    @Bean
    @Override // 우리가 여기서 다시 정의했으니까 달아준다.
    public AuthenticationManager authenticationManager() throws Exception { // 보안,권한과 관련된 매니저같은 느낌이다.
        return super.authenticationManagerBean(); // 슈퍼맨, 나의 매니저, 부모는 위 extends하고 있죠? 만약에 매니저역할 못하면 어떻게해요? = exception

    }

    // 퍼미션이 무엇인지 보기.
    @Override // 포함되어 있는 기능을 재정의 하고 있어서 달아주는 것임
    protected void configure(HttpSecurity httpSecurity) throws Exception { // 설정에 문제가 생기거나, 예상치 못한 에러가 생기면 무조건 멈춰줘야함 = exception
        httpSecurity
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                    .authorizeRequests()
                        .antMatchers(HttpMethod.GET, "/exception/**").permitAll() // 전체 허용
                // 로그인 API
                        .antMatchers("/v1/login/**").permitAll() // 전체허용
                // 권한 테스트 API
                        .antMatchers("/v1/auth-test/test-admin").hasAnyRole("ADMIN")
                        .antMatchers("/v1/auth-test/test-user").hasAnyRole("USER")
                        .antMatchers("/v1/auth-test/test-all").hasAnyRole("ADMIN", "USER")
                        .antMatchers("/v1/auth-test/login-all/**").hasAnyRole("ADMIN", "USER")
                // 회원 API
                        .antMatchers("/v1/member/join").permitAll() // [일반유저/관리자] 회원등록(C)
                        .antMatchers("/v1/member/nickname").hasAnyRole("USER") // [일반유저] 닉네임 조회(R)
                        .antMatchers("/v1/member/nickname-change").hasAnyRole("USER") // [일반유저] 닉네임 수정(U)
                        .antMatchers("/v1/member/information/**").hasAnyRole("ADMIN") // [관리자] 회원정보 리스트 조회(R)
                // 산책장소 API
                        .antMatchers("/v1/walking-address/new").hasAnyRole("ADMIN") // [관리자] 산책장소 등록(C)
                        .antMatchers("/v1/walking-address/data").hasAnyRole("ADMIN", "USER") // [일반유저] 산책장소 리스트 조회(R)
                        .antMatchers("/v1/walking-address/data/**").hasAnyRole("ADMIN") // [관리자] 산책장소 수정(U)
                        .antMatchers("/v1/walking-address/favorites-new").hasAnyRole("USER") // [일반유저] 나의 즐겨찾기 장소 등록/수정(C/U)
                        .antMatchers("/v1/walking-address/favorites-data").hasAnyRole("USER") // [일반유저] 나의 즐겨찾기 장소 리스트 조회(R)
                //공지사항 API
                        .antMatchers("/v1/notice/new").hasAnyRole("ADMIN") // [관리자] 공지사항 등록(C)
                        .antMatchers("/v1/notice/list/all").hasAnyRole("ADMIN") // [관리자] 전체 공지사항 조회(R)
                        .antMatchers("/v1/notice/list/note/all").hasAnyRole("ADMIN") // [관리자] 전체 공지사항 조회 + 내용 (R)
                        .antMatchers("/v1/notice/list/search/**").hasAnyRole("ADMIN") // [관리자] 전체 공지사항 기간별 조회 + 제목 검색(R)
                        .antMatchers("/v1/notice/list/page/**").hasAnyRole("ADMIN", "USER") // [관리자/일반유저] 유효 공지사항 기간별 조회(R)
                        .antMatchers("/v1/notice/detail/**").hasAnyRole("ADMIN") // [관리자] 개별 항목 조회(R)
                        .antMatchers("/v1/notice/put/**").hasAnyRole("ADMIN") // [관리자] 공지사항 수정(U)
                        .antMatchers("/v1/notice/enable/**").hasAnyRole("ADMIN") // [관리자] 공지사항 게시여부 수정(U)
                //키워드 API
                        .antMatchers("/v1/keyword/new").hasAnyRole("ADMIN", "USER") // [관리자/일반유저] 키워드 등록(C)
                        .antMatchers("/v1/keyword/list/walking").hasAnyRole("ADMIN", "USER") // [관리자/일반유저] 키워드-산책관 리스트 조회(R)
                        .antMatchers("/v1/keyword/list/friend-i-want").hasAnyRole("ADMIN", "USER") // [관리자/일반유저] 키워드-나는 이런친구가 좋아요 리스트 조회(R)
                        .antMatchers("/v1/keyword/list/friend-you-want").hasAnyRole("ADMIN", "USER") // [관리자/일반유저] 키워드-나는 이런친구가 될게요 리스트 조회(R)
                        .antMatchers("/v1/keyword/put/walking").hasAnyRole("ADMIN", "USER") // [관리자/일반유저] 키워드-산책관 수정(U)
                        .antMatchers("/v1/keyword/put/friend-i-want").hasAnyRole("ADMIN", "USER") // [관리자/일반유저] 키워드-나는 이런친구가 좋아요 수정(U)
                        .antMatchers("/v1/keyword/put/friend-you-want").hasAnyRole("ADMIN", "USER") // [관리자/일반유저] 키워드-나는 이런친구가 될게요 수정(U)
                // 펫 API
                        .antMatchers("/v1/pet/**").hasAnyRole("USER")
                // 매칭내역 API
                        .antMatchers("/v1/matching-usage/new").hasAnyRole("USER") // [일반유저] 매칭 신청(C)
                        .antMatchers("/v1/matching-usage/my-apply").hasAnyRole("USER") // [일반유저] 내가 신청한 매칭내역(R)
                        .antMatchers("/v1/matching-usage/my-receive").hasAnyRole("USER") // [일반유저] 내가 받은 매칭내역(R)
                        .antMatchers("/v1/matching-usage/my-accept").hasAnyRole("USER") // [일반유저] 나의 매칭 수락(U)
                // 기본 접근 권한
                        .anyRequest().hasRole("ADMIN") // 기본 접근 권한은 ROLE_ADMIN
                .and()
                    .exceptionHandling().accessDeniedHandler(new CustomAccessDeniedHandler())
                .and()
                    .exceptionHandling().authenticationEntryPoint(new CustomAuthenticationEntryPoint())
                .and()
                    .addFilterBefore(new JwtTokenFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source =
                new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOriginPattern("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
}
