package com.amh.sansomatchingwalkingapi.controller.matching_usage;

import com.amh.sansomatchingwalkingapi.entity.Member;
import com.amh.sansomatchingwalkingapi.entity.WalkingAddress;
import com.amh.sansomatchingwalkingapi.model.common.CommonResult;
import com.amh.sansomatchingwalkingapi.model.common.ListResult;
import com.amh.sansomatchingwalkingapi.model.matchingusage.MatchingCreateRequest;
import com.amh.sansomatchingwalkingapi.model.matchingusage.MyMatchingAcceptRequest;
import com.amh.sansomatchingwalkingapi.model.matchingusage.MyMatchingListResponse;
import com.amh.sansomatchingwalkingapi.service.MatchingUsageService;
import com.amh.sansomatchingwalkingapi.service.MemberDataService;
import com.amh.sansomatchingwalkingapi.service.ProfileService;
import com.amh.sansomatchingwalkingapi.service.WalkingAddressService;
import com.amh.sansomatchingwalkingapi.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "매칭내역 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/matching-usage")
public class MatchingUsageController {

    private final MatchingUsageService matchingUsageService;
    private final ProfileService profileService;
    private final MemberDataService memberDataService;

    private final WalkingAddressService walkingAddressService;
    @ApiOperation(value = "[일반유저] 매칭 신청")
    @PostMapping("/new")
    public CommonResult setMatching(@RequestBody @Valid MatchingCreateRequest request) {
        Member applyMember = profileService.getMemberData();
        Member receiveMember = memberDataService.getMember(request.getReceiveMemberId());
        WalkingAddress walkingAddress = walkingAddressService.getWalkingAddress(request.getWalkingAddressId());
        matchingUsageService.setMatching(applyMember, receiveMember, walkingAddress);
        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "[일반유저] 내가 신청한 매칭내역")
    @GetMapping("/my-apply")
    public ListResult<MyMatchingListResponse> getMyMatchingApplyList() {
        Member Member = profileService.getMemberData();
        return ResponseService.getListResult(matchingUsageService.getMyMatchingApplyList(Member),true);
    }
    @ApiOperation(value = "[일반유저] 내가 받은 매칭내역")
    @GetMapping("/my-receive")
    public ListResult<MyMatchingListResponse> getMyMatchingReceiveList() {
        Member Member = profileService.getMemberData();
        return ResponseService.getListResult(matchingUsageService.getMyMatchingReceiveList(Member),true);
    }
    @ApiOperation(value = "[일반유저] 나의 매칭 수락")
    @PutMapping("/my-accept")
    public CommonResult putMyMatchingAccept(@RequestBody @Valid MyMatchingAcceptRequest request) {
        matchingUsageService.putMyMatchingAccept(request);
        return ResponseService.getSuccessResult();
    }

}
