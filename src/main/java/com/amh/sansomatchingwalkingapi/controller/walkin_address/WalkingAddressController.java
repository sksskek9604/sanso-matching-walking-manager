package com.amh.sansomatchingwalkingapi.controller.walkin_address;

import com.amh.sansomatchingwalkingapi.entity.Member;
import com.amh.sansomatchingwalkingapi.model.common.CommonResult;
import com.amh.sansomatchingwalkingapi.model.common.ListResult;
import com.amh.sansomatchingwalkingapi.model.walkingaddress.WalkingAddressAdminRequest;
import com.amh.sansomatchingwalkingapi.model.walkingaddress.WalkingAddressAdminResponse;
import com.amh.sansomatchingwalkingapi.model.walkingaddress.WalkingAddressUserFavoritesRequest;
import com.amh.sansomatchingwalkingapi.model.walkingaddress.WalkingAddressUserFavoritesResponse;
import com.amh.sansomatchingwalkingapi.service.ProfileService;
import com.amh.sansomatchingwalkingapi.service.WalkingAddressService;
import com.amh.sansomatchingwalkingapi.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "산책장소 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/walking-address")
public class WalkingAddressController {
    private final WalkingAddressService walkingAddressService;
    private final ProfileService profileService;

    @ApiOperation(value = "[관리자] 산책장소 등록")
    @PostMapping("/new")
    public CommonResult setWalkingAddress(@RequestBody @Valid WalkingAddressAdminRequest request) {
        walkingAddressService.setWalkingAddress(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "[일반유저] 산책장소 전체 리스트 조회")
    @GetMapping("/data")
    public ListResult<WalkingAddressUserFavoritesResponse> getWalkingAddresses() {
        return ResponseService.getListResult(walkingAddressService.getWalkingAddresses(),true);
    }

    @ApiOperation(value = "[관리자] 산책장소 수정")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "walkingAddressId", value = "산책 장소 시퀀스", required = true)
    )
    @PutMapping("/data/{walkingAddressId}")
    public CommonResult putWalkingAddress(@PathVariable long walkingAddressId, @RequestBody @Valid WalkingAddressAdminRequest request) {
        walkingAddressService.putWalkingAddress(walkingAddressId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "[일반유저] 나의 즐겨찾기 장소 등록/수정")
    @PutMapping("/favorites-new")
    public CommonResult putMyWalkingAddressFavorites(@RequestBody @Valid WalkingAddressUserFavoritesRequest request) {
        Member member = profileService.getMemberData();
        walkingAddressService.putMyWalkingAddressFavorites(member, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "[일반유저] 나의 즐겨찾기 장소 리스트 조회")
    @GetMapping("/favorites-data")
    public ListResult<WalkingAddressAdminResponse> getMyWalkingAddressFavorites() {
        Member member = profileService.getMemberData();
        return ResponseService.getListResult(walkingAddressService.getMyWalkingAddressFavorites(member),true);
    }
}
