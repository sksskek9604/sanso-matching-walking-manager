package com.amh.sansomatchingwalkingapi.entity;

import com.amh.sansomatchingwalkingapi.interfaces.CommonModelBuilder;
import com.amh.sansomatchingwalkingapi.model.walkingaddress.WalkingAddressAdminRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

import static com.amh.sansomatchingwalkingapi.lib.CommonDate.getNowDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class WalkingAddress {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // 시퀀스
    @Column(nullable = false, length = 20)
    private String walkingAddressName; // 산책 장소
    @Column(nullable = false)
    private Double latitude; // 위도
    @Column(nullable = false)
    private Double longitude; // 경도
    @Column(nullable = false)
    private LocalDateTime dateCreate; // 등록시간
    @Column(nullable = false)
    private LocalDateTime dateUpdate; // 수정시간

    public void putWalkingAddress(WalkingAddressAdminRequest request) {
        this.walkingAddressName = request.getWalkingAddressName();
        this.latitude = request.getLatitude();
        this.longitude = request.getLongitude();
        this.dateUpdate = getNowDateTime();
    }
    private WalkingAddress(WalkingAddressBuilder builder) {
        this.walkingAddressName = builder.walkingAddressName;
        this.latitude = builder.latitude;
        this.longitude = builder.longitude;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }
    public static class WalkingAddressBuilder implements CommonModelBuilder<WalkingAddress> {
        private final String walkingAddressName; // 산책 장소
        private final Double latitude; // 위도
        private final Double longitude; // 경도
        private final LocalDateTime dateCreate; // 등록시간
        private final LocalDateTime dateUpdate; // 수정시간

        public WalkingAddressBuilder(WalkingAddressAdminRequest request) {
            this.walkingAddressName = request.getWalkingAddressName();
            this.latitude = request.getLatitude();
            this.longitude = request.getLongitude();
            this.dateCreate = getNowDateTime();
            this.dateUpdate = getNowDateTime();
        }
        @Override
        public WalkingAddress build() {
            return new WalkingAddress(this);
        }
    }




}
