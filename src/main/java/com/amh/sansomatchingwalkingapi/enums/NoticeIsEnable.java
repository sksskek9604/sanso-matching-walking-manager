package com.amh.sansomatchingwalkingapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum NoticeIsEnable {

    NOTICE_ENABLE("공지중", true)
    ,NOTICE_DISABLE("해제중", false)
    ;

    private final String name;
    private final Boolean type;


}
