package com.amh.sansomatchingwalkingapi.enums.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {

    SUCCESS(0, "성공하였습니다.")
    , FAILED(-1, "실패하였습니다.")

    , ACCESS_DENIED(-1000, "권한이 없습니다.")
    , USERNAME_SIGN_IN_FAILED(-1001, "가입된 사용자가 아닙니다.")
    , AUTHENTICATION_ENTRY_POINT(-1002, "접근 권한이 없습니다.")

    , MISSING_DATA(-10000, "데이터를 찾을 수 없습니다.")
    , USERNAME_TYPE_ERROR(-10002, "유효한 아이디 형식이 아닙니다.")
    , LOGIN_PASSWORD_FAILED(-10003, "비밀번호가 틀렸습니다.")
    , USERNAME_USING(-10004, "아이디가 사용중 입니다.")
    , WRONG_PHONE_NUMBER(-10005, "잘못된 핸드폰 번호입니다.")
    , PHONE_NUMBER_USING(-10005, "이미 등록된 핸드폰 번호입니다.")


    , NO_EQUAL_CHECK(-20000, "중복 체크가 불가능합니다.")
    , NICKNAME_USING(-20001, "닉네임이 사용중 입니다.")
    , NICKNAME_OVERLAP(-20002, "동일한 닉네임으로는 변경이 불가합니다.")

    , APPLY_MEMBER_OVERLAP(-30001, "자신에게는 신청 할 수 없습니다.")
    ;

    private final Integer code;
    private final String msg;
}
