package com.amh.sansomatchingwalkingapi.exception;

public class CAccessDeniedException extends RuntimeException  {

    public CAccessDeniedException(String msg, Throwable t) {
        super(msg, t); //위에 두개 받아줄 것.
    }

    public CAccessDeniedException(String msg) {
        super(msg);
    }

    // 3번째는 빈바구니
    public CAccessDeniedException() {
        super();
    }
}
