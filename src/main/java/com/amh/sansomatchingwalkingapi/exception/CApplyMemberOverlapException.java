package com.amh.sansomatchingwalkingapi.exception;

public class CApplyMemberOverlapException extends RuntimeException {

    public CApplyMemberOverlapException(String msg, Throwable t) {
        super(msg, t); //위에 두개 받아줄 것.
    }

    public CApplyMemberOverlapException(String msg) {
        super(msg);
    }

    // 3번째는 빈바구니
    public CApplyMemberOverlapException() {
        super();
    }
}
