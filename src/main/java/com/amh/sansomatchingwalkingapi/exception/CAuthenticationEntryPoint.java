package com.amh.sansomatchingwalkingapi.exception;

public class CAuthenticationEntryPoint extends RuntimeException {

    public CAuthenticationEntryPoint(String msg, Throwable t) {
        super(msg, t); //위에 두개 받아줄 것.
    }

    public CAuthenticationEntryPoint(String msg) {
        super(msg);
    }

    // 3번째는 빈바구니
    public CAuthenticationEntryPoint() {
        super();
    }
}
