package com.amh.sansomatchingwalkingapi.exception;

public class CPhoneNumberUsingException extends RuntimeException {
    public CPhoneNumberUsingException(String msg, Throwable t) {
        super(msg, t);
    }

    public CPhoneNumberUsingException(String msg) {
        super(msg);
    }

    public CPhoneNumberUsingException() {
        super();
    }
}
