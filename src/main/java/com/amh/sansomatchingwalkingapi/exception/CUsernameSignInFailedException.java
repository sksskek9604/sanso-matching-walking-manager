package com.amh.sansomatchingwalkingapi.exception;

public class CUsernameSignInFailedException extends RuntimeException {

    public CUsernameSignInFailedException(String msg, Throwable t) {
        super(msg, t); //위에 두개 받아줄 것.
    }

    public CUsernameSignInFailedException(String msg) {
        super(msg);
    }

    // 3번째는 빈바구니
    public CUsernameSignInFailedException() {
        super();
    }
}
