package com.amh.sansomatchingwalkingapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
