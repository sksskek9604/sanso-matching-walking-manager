package com.amh.sansomatchingwalkingapi.lib;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class CommonDate {
    public static LocalDate getNowDate() {
        LocalDateTime nowTime = LocalDateTime.now().plusHours(9);
        return LocalDate.of(nowTime.getYear(), nowTime.getMonth(), nowTime.getDayOfMonth());
    }

    public static LocalDateTime getNowDateTime() {
        return LocalDateTime.now().plusHours(9);
    }
    // 헷갈려서 getNowTime -> getNowDateTime 으로 변경
    public static LocalTime getNowOnlyTime() {
        return LocalTime.now().plusHours(9);
    }
}
