package com.amh.sansomatchingwalkingapi.model.common;

import com.amh.sansomatchingwalkingapi.model.common.CommonResult;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SingleResult<T> extends CommonResult {
    private T data;
}
