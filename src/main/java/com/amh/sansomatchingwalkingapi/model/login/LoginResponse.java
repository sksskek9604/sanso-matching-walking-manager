package com.amh.sansomatchingwalkingapi.model.login;

import com.amh.sansomatchingwalkingapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginResponse {
    @ApiModelProperty(notes = "토큰")
    private String token;

    @ApiModelProperty(notes = "닉네임")
    private String nickName;


    private LoginResponse(LoginResponseBuilder builder) {
        this.token = builder.token;
        this.nickName = builder.nickName;

    }

    public static class LoginResponseBuilder implements CommonModelBuilder<LoginResponse> {
        private final String token;

        private final String nickName;


        public LoginResponseBuilder(String token, String nickName) {
            this.token = token;
            this.nickName = nickName;
        }

        @Override
        public LoginResponse build() {
            return new LoginResponse(this);
        }
    }

}
