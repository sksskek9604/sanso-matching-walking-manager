package com.amh.sansomatchingwalkingapi.model.matchingusage;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MatchingCreateRequest {

    @ApiModelProperty(notes = "신청받은 회원 시퀀스")
    @NotNull
    private Long receiveMemberId;

    @ApiModelProperty(notes = "산책장소 시퀀스")
    @NotNull
    private Long walkingAddressId;

}
