package com.amh.sansomatchingwalkingapi.model.walkingaddress;

import com.amh.sansomatchingwalkingapi.entity.WalkingAddress;
import com.amh.sansomatchingwalkingapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * [일반유저] 즐겨찾기 장소 조회 모델
 * 내가 즐겨찾는 산책로 보여주는 용도
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class WalkingAddressUserFavoritesResponse {
    @ApiModelProperty(value = "산책장소 시퀀스")
    private Long id;
    @ApiModelProperty(value = "산책장소")
    private String walkingAddressName;
    public WalkingAddressUserFavoritesResponse(WalkingAddressFavoritesResponseBuilder builder) {
        this.id = builder.id;
        this.walkingAddressName = builder.walkingAddressName;
    }
    public static class WalkingAddressFavoritesResponseBuilder implements CommonModelBuilder<WalkingAddressUserFavoritesResponse> {
        private final Long id;
        private final String walkingAddressName;

        public WalkingAddressFavoritesResponseBuilder(WalkingAddress walkingAddress) {
            this.id = walkingAddress.getId();
            this.walkingAddressName = walkingAddress.getWalkingAddressName();
        }

        @Override
        public WalkingAddressUserFavoritesResponse build() {
            return new WalkingAddressUserFavoritesResponse(this);
        }
    }
}
