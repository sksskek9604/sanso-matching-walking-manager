package com.amh.sansomatchingwalkingapi.respository;

import com.amh.sansomatchingwalkingapi.entity.Keyword;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface KeywordRepository extends JpaRepository<Keyword, Long> {
    List<Keyword> findAllByMember_Id(Long memberId);

}
