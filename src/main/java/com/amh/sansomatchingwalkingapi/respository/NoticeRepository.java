package com.amh.sansomatchingwalkingapi.respository;

import com.amh.sansomatchingwalkingapi.entity.Notice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface NoticeRepository extends JpaRepository<Notice, Long> {

    List<Notice> findAllByNoticeIsEnableAndDatePostGreaterThanEqualAndDatePostLessThanEqualOrderByIdDesc(Boolean noticeIsEnable, LocalDate dateStart, LocalDate dateEnd);

    Page<Notice> findAllByTitleContainingIgnoreCaseAndDatePostGreaterThanEqualAndDatePostLessThanEqualOrderByNoticeIsEnableDescIdDesc(String searchTitle, LocalDate dateStart, LocalDate dateEnd, Pageable pageable);

    Page<Notice> findAllByTitleContainingIgnoreCaseAndNoticeIsEnableAndDatePostGreaterThanEqualAndDatePostLessThanEqualOrderByIdDesc(String searchTitle, Boolean noticeIsEnable, LocalDate dateStart, LocalDate dateEnd, Pageable pageable);


}
