package com.amh.sansomatchingwalkingapi.respository;

import com.amh.sansomatchingwalkingapi.entity.WalkingAddress;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WalkingAddressRepository extends JpaRepository<WalkingAddress, Long> {
    List<WalkingAddress> findAllByIdEqualsOrIdEqualsOrId (long walkingAddressId1, long walkingAddressId2, long walkingAddressId3);
}
