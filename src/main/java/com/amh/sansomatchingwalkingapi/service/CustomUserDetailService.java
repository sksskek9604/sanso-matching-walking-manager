package com.amh.sansomatchingwalkingapi.service;

import com.amh.sansomatchingwalkingapi.entity.Member;
import com.amh.sansomatchingwalkingapi.exception.CUsernameSignInFailedException;
import com.amh.sansomatchingwalkingapi.respository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService { // 검사하는 인력(기능)만들기 + 시큐리티 패키지에서 가이드 가져옴

    private final MemberRepository memberRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Member member = memberRepository.findByUsername(username).orElseThrow(CUsernameSignInFailedException::new);
        return member;
    }
}
