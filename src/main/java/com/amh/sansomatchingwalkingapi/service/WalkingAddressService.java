package com.amh.sansomatchingwalkingapi.service;

import com.amh.sansomatchingwalkingapi.entity.Member;
import com.amh.sansomatchingwalkingapi.entity.WalkingAddress;
import com.amh.sansomatchingwalkingapi.exception.CMissingDataException;
import com.amh.sansomatchingwalkingapi.model.common.ListResult;
import com.amh.sansomatchingwalkingapi.model.walkingaddress.WalkingAddressAdminResponse;
import com.amh.sansomatchingwalkingapi.model.walkingaddress.WalkingAddressUserFavoritesRequest;
import com.amh.sansomatchingwalkingapi.model.walkingaddress.WalkingAddressUserFavoritesResponse;
import com.amh.sansomatchingwalkingapi.model.walkingaddress.WalkingAddressAdminRequest;
import com.amh.sansomatchingwalkingapi.respository.MemberRepository;
import com.amh.sansomatchingwalkingapi.respository.WalkingAddressRepository;
import com.amh.sansomatchingwalkingapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class WalkingAddressService {
    private final WalkingAddressRepository walkingAddressRepository;
    private final MemberRepository memberRepository;

    /**
     * [관리자] 산책장소 등록 메서드
     * @param request 산책 장소 정보 (장소, 위도, 경도)
     */
    public void setWalkingAddress(WalkingAddressAdminRequest request) {
        WalkingAddress walkingAddress = new WalkingAddress.WalkingAddressBuilder(request).build();
        walkingAddressRepository.save(walkingAddress);
    }

    /**
     *[일반유저] 신청한 산책장소 정보 가져오기
     * @param walkingAddressId 신청한 산책장소 시퀀스
     * @return 신청한 산책장소 정보
     */
    public WalkingAddress getWalkingAddress(long walkingAddressId) {
        return walkingAddressRepository.findById(walkingAddressId).orElseThrow(CMissingDataException::new);
    }

    /**
     * [일반유저] 전체 산책장소 조회 메서드
     * 즐겨찾기 고르는 용도
     * @return 산책 장소 리스트
     */
    public ListResult<WalkingAddressUserFavoritesResponse> getWalkingAddresses() {

        List<WalkingAddress> walkingAddresses = walkingAddressRepository.findAll();

        List<WalkingAddressUserFavoritesResponse> result = new LinkedList<>();

        walkingAddresses.forEach(walkingAddress -> {
            WalkingAddressUserFavoritesResponse addressItem = new WalkingAddressUserFavoritesResponse.WalkingAddressFavoritesResponseBuilder(walkingAddress).build();

            result.add(addressItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * [관리자] 산책장소 수정 메서드
     * @param walkingAddressId 산책 장소 시퀀스
     */
    public void putWalkingAddress(long walkingAddressId, WalkingAddressAdminRequest request) {
        WalkingAddress walkingAddress = walkingAddressRepository.findById(walkingAddressId).orElseThrow(CMissingDataException::new);
        walkingAddress.putWalkingAddress(request);
        walkingAddressRepository.save(walkingAddress);
    }

    /**
     * [일반유저] 나의 즐겨찾는 장소 등록/수정 메서드
     * @param member 회원 시퀀스
     * @param request 즐겨찾기 장소 3곳
     */
    public void putMyWalkingAddressFavorites(Member member, WalkingAddressUserFavoritesRequest request) {
        member.putMyWalkingAddressFavorites(request);
        memberRepository.save(member);
    }

    /**
     * [일반유저] 나의 즐겨찾는 장소 조회 메서드
     * @param member 회원 시퀀스
     * @return 즐겨찾기 장소 3곳 리스트
     */
    public ListResult<WalkingAddressAdminResponse> getMyWalkingAddressFavorites(Member member) {
        List<WalkingAddress> walkingAddresses = walkingAddressRepository
                .findAllByIdEqualsOrIdEqualsOrId(
                        member.getWalkingAddressId1(),
                        member.getWalkingAddressId2(),
                        member.getWalkingAddressId3()
                );

        List<WalkingAddressAdminResponse> result = new LinkedList<>();

        walkingAddresses.forEach(walkingAddress -> {
            WalkingAddressAdminResponse addressItem = new WalkingAddressAdminResponse.WalkingAddressResponseBuilder(walkingAddress).build();

            result.add(addressItem);
        });

        return ListConvertService.settingResult(result);
    }

}
